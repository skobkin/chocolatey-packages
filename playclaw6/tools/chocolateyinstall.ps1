﻿$ErrorActionPreference = 'Stop'; # stop on all errors
$toolsDir   = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"

$url        = https://www.playclaw.com/bin/playclaw6-5090.exe'' # download url, HTTPS preferred

$packageArgs = @{
  packageName   = $env:ChocolateyPackageName
  unzipLocation = $toolsDir
  fileType      = 'exe'
  url           = $url
  url64bit      = $url64

  softwareName  = 'playclaw6*' #part or all of the Display Name as you see it in Programs and Features. It should be enough to be unique

  checksum      = '4E9D01E052F77290830FE6E21FD3B0AABCB5EFAE7B601E45EFF346E6F4F63B8A'
  checksumType  = 'sha256' #default is md5, can also be sha1, sha256 or sha512

  silentArgs   = '/VERYSILENT /SUPPRESSMSGBOXES /NORESTART /SP-' # Inno Setup
  # http://www.jrsoftware.org/ishelp/index.php?topic=setupexitcodes
  validExitCodes= @(0)
}

Install-ChocolateyPackage @packageArgs # https://chocolatey.org/docs/helpers-install-chocolatey-package

# TODO: disabling shortcuts parameters passing