[![pipeline status](https://gitlab.com/skobkin/chocolatey-packages/badges/master/pipeline.svg)](https://gitlab.com/skobkin/chocolatey-packages/commits/master)

[![Chocolatey logo](https://gitlab.com/skobkin/chocolatey-packages/raw/master/images/chocolatey-logo2.png)](https://chocolatey.org/)

# My chocolatey packages

You can find collection of [Chocolatey](https://chocolatey.org/) Windows [package manager](https://en.wikipedia.org/wiki/Package_manager) packages in this repository.

# List of packages

- [PlayClaw 6](https://playclaw.com/)
